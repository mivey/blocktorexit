#!/usr/bin/env python

""" block-tor-exit.py: This script simply adds tor exitnodes to your iptables for dropping traffic. """

__author__ = "Mike Ivey"
__version__ = "0.0.1"
__maintainer__ = "Mike Ivey"
__status__ = "Production"

import urllib2
import subprocess
import time
import re

class Tor:
    def __init__(self):
        self.tor_ips = None        

    def set_ip_list(self):
        self.tor_ips = urllib2.urlopen('http://torstatus.blutmagie.de/ip_list_exit.php/Tor_ip_list_EXIT.csv')

    def get_ip_list(self):
        return self.tor_ips

    def set_iptable_rule(self):
        #Try to create BLACKLIST chain
        cmd = "iptables -N BLACKLIST"
        proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print "Trying to create Blacklist"
        print "Output:", proc.stdout.read()
        print "Error:", proc.stderr.read()
        
        #Flush ip addresses from BLACKLIST chain
        cmd = "iptables -F BLACKLIST"
        proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print "Flushing ip's from BLACKLIST"
        print "Output:", proc.stdout.read()
        print "Error:", proc.stderr.read()
        
        # Remove BLACKLIST rule just incase it is no longer at the top of iptables INPUT Chain
        cmd = "iptables -D INPUT ${BLAH=`sudo iptables -L INPUT --line-numbers | grep BLACKLIST | awk '{print $1}'`}"
        proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print "Reordering BLACKLIST"
        print "Output:", proc.stdout.read()
        print "Error:", proc.stderr.read()
        
        # Add BLACKLIST DROP rule to the top of iptables INPUT Chain
        cmd = "iptables -I INPUT 1 -j BLACKLIST"
        proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print "Added BLacklist"
        print "Output:", proc.stdout.read()
        print "Error:", proc.stderr.read()
        
    def get_iptable_rule(self):
        cmd = "iptables -L INPUT --line-numbers | grep BLACKLIST"
        proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print "The following rules are in iptables INPUT chain"
        print "Output:", proc.stdout.read()
        print "Error:", proc.stderr.read()


    def add_iptable_rule(self):
        for ip in self.tor_ips.readlines():
            clean_ip = re.findall(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ip)
            for newip in clean_ip:
                print newip
                time.sleep(0.2)
                cmd = "iptables -A BLACKLIST -s " + newip + " -j DROP"
                proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


    def remove_iptable_rule(self):
        pass


def main():
    torblock = Tor()
    torblock.set_ip_list()
    torblock.set_iptable_rule()
    torblock.get_iptable_rule()
    torblock.add_iptable_rule()

if __name__ == "__main__":
    main()
